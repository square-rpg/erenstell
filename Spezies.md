# Spezies

## Lotra
### Beschreibung
Die Lotra sind humanoid mit hell bis dunkelgrauer Haut und haben keinen sichtbaren Haarwuchs. Ihr Gesicht ist geprägt von einem leicht verlängerten Schädel dessen Scheitelbeine spitz zulaufen und dem Kopf eine Dreiecksform geben. Ebenso auffällig sind ihre mandelförmige Augen, welche in verschiedenen Grautönen schimmern und in denen sich immer präsente Wachsamkeit spiegelt. Viele der großen Denker Erenstells waren Lotra; in jede der grossen Schulen und Werkstätten findet sich zumindest ein prominenter Lotra. Dennoch wird den Lotra auch nachgesagt, dass sie manchmal fast ein wenig zu klug für ihr eigenes Wohl sind.

**Vorurteile**
- Arrogant und Überheblich

**Besondere Begabung**
- Mentale Gewandtheit

## Oktaissa
### Beschreibung
Die Oktaissa sind Humanoid mit dichtem, kurzem Fell. Die Ohren sind gewöhnlich gross, rund und - abgesehen von fusseligem Flaum - kahl, obwohl es Familien aus dem Norden mit spitzen, behaarten Ohren gibt. Die Oktaissa sind strikte Vegetarier und leben in großen Familien im Kern der Stadt. Sie sind geschickte Händler und Handwerker. Seit Urzeiten gibt es eine tiefe Feindschaft zwischen den Oktaissa und den Natassen; es ist eine tief verwurzelte Angst der Oktaissa, dass ein Natasse ihre Kinder stehlen könnte. Durch ihre starken Bindungen durch und über die Familien hinaus, geniessen die Oktaissa großen Einfluss in Erenstell.

**Vorurteile**
- Raffgierig und Manipulierend

**Besondere Begabung**
- Emotionale Gewandtheit

## Hanutu
### Beschreibung
Die Hanutu zeichnen sich durch ihre ockerrote bis graue, dicke und lederartige Haut aus, welche eine schützende, gummige Substanz absondert. Diese schützt die Hanutu vor den Elementen der Welt schützt, wie zum Beispiel grosser Hitze oder Kälte, aber auch beissende Substanzen machen ihnen viel weniger aus als anderen Kreaturen. Ihre Hände sowie Füsse sind besonders dick gepanzert, was ihnen erlaubt, selbst die härtesten Aufgaben zu bewältigen. Die Haut aller Hanatu bildet zudem Kalziumabsonderungen; bei manchen formt sich sogar eine richtige, dicke Schale, deren Beschaffenheit von Umweltumstände abhängt. Zum Beispiel brüsten sich die Panzerschnegel aus den ehernen Zähne - eine gewaltige Bergformation mit höllisch spitzen Gipfeln bekannt für ihre Eisenvorkommen - mit ihren "stählernen" Schalen.

**Vorurteile**
- Stur und dumm

**Besondere Begabung**
- Physische Ausdauer

## Natassen
### Beschreibung
Natassen sind humanoid, mit kurzem, dichten Haar und spitz zulaufender Schnauze mit spitzen Zähnen. Die Farbe ihres Haares variiert von dunklem grau bis eiweiss. Auch rostrot ist vorhanden. Die Fellzeichnungen sind divers. Ihre beweglichen Ohren sind spitz und behaart. Natassen sind strikte Karnivoren. Sie haben schlanke Körper und geschickte Finger, weswegen sie - zu Recht oder Unrecht - häufig mit Diebstahls und Täuschung in Verbindung gebracht werden. Ihre Augen sind lebhaft und neugierig und selten trifft man einen desinteressierten Natassen.

**Vorurteile**
- Hinterhältig und Verschlagen

**Besondee Begabung**
- Physische Agilität

## P'tha
### Beschreibung
Die P'tha haben massive, schwere Körper und grosse Köpfe mit einer einem agilen Rüssel. Manche P'tha haben auch grosse Stosszähne, obwohl Balzrituale inzwischen größtenteils ohne sie auskommen. Ihre unbehaarte Haut ist von einer grauen, dicken Textur. Durch sie kommen die P'tha gut mit großer Hitze zurecht, leiden jedoch in der Kälte. Das kulturelle Gedächnis der P'tha geht weiter zurück als das aller anderen Spezies und es gibt historisch belegte Erzählungen der P'tha aus den frühsten Zeiten. Dieses starke Gedächnis zeichnet sich auch bei den individuellen P'tha ab; ein Vereinbarung, geschlossen unter Aufsicht eines geachtetn P'tha, ist in den Gerichten kaum anfechtbar.

**Vorurteile**
- Nachtragend und Besserwisserisch

**Besondere Begabung**
- Mentale Stärke 

## Narogok
### Beschreibung
Die Narogok sind kraftvolle und muskulöse Wesen mit blassrosa bis grauer Haut. Sie haben kein Fell, stattdessen ist ihre Haut von dicken borstigen Stoppeln bedeckt. Ihre grossen, kurzen und runde Schnauzen haben grosse, fast lippenlose Münder mit ebenfalls grossen und flachen Zähnen. Die Nasenlöcher sind gross, rund und nach vorne gerichtet, die Ohren sind klein und knubbelig. Die Muskulatur der Narogok ist ausgeprägt und sie haben breite Schultern und kräftige Arme. Ihre Haut ist von einer groben, ledrigen Textur mit einer dicken Fettschicht darunter. Mit ihren kräftigen Händen können sie mühelos schwere Lasten tragen und sie sind für ihre erstaunliche Schnelligkeit bekannt - wenn sie einmal in Fahrt kommen. Die Narogok stellen seit langem die Elite unter den Verteidigern Erenstells, zumindest was die Gardisten der Konzilwache oder die Soldaten der Miliz angeht.

**Vorurteile**
- Dumm und Leichtgläubig

**Besondere Begabung**
- Physische Stärke

