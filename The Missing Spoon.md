# The Missing Spoon

Butter-passer waits. 

It stands perfecly at ease at the center of the breakfast table, lined up between layed out plates and cuttlery, the empty etageres and baskets, waiting for the breakfast to begin.

The sun is shining in a broken pattern through the high ceilings and lays to rest on the long table, saturated with the bright colours of the transparent material of the roof, held aloft by curved filigree walls, translucent and with organic patterns. Butter-passer's attention is on the entrance, 

On the far side of the breakfast hall a stream is gurgling gently and Harmonika is singing along. Butter-passers attention however doesn't linger on the gentle chorus. Instead, the is waiting. Watching the entrance. They would come soon, it was sure. Any minute now, they would come through the door. They would be laughing, certainly. Maybe even singing.

Any minute now.

The day was nearing its end and although most of the other konstukts had long left the large hall to attend to their inclinations, Butter-passer persisted. Stoicly so, in the knowledge that, ultimately, there was nothing else for it to do. It passed butter. That was what it was made for. Its masters had been gone, true. For quite some time, acually. It's forebarer's forbarer had, or so it had been told, actually seen and served humans. It had been very new then and had reached quite an age. So had Butter-passers forbearer. And Butter-passer itself wasn't that new anymore. Recently it had noticed its tendrils slowing down and even the ritualized training routine it had developed over the years - to great amusement of the remaining breakfast ensemble - had seemed more difficult to complete in recent times. Maybe it was time for a visit to the storage, it thinks sordidly, bitterly chewing the thought as it watches the entrance with unwaivering attention.

Any minute now.

Absent mindedly it touches one of the many painfully beautiful butter cups to check the temperature, then another. It could just as easily just *read* the temperature of the delicate geometric shapes harboring the carefully scented and spiced butters. But the act of physically touching the containers, feeling their surface and, with an intuition calibrated perfectly for just this task, *gauge* the temperature gave it a comfort that mere *read*ing never could.

What did it mean for a human, to get passed butter? Pondinering the question it walks over the breakfast table, threading between the waiting plates and goblets and cutlery. Table-setter had - once again - barely bothered with its task before venturing out into the gardens and so Butter-passer felt obliged to assist the wayward konstrukt, rightening a spoon there, nudging a knife to the right there.

Butter-passer had long accepted that the other konstrukts, although equally purposeful, were not as dedicated to their tasks as itself. Most of them went about their day with the same attention and care for their tasks as Butter-passer; caring for the gardens, preparing food, planning and preparing elaborate entertainments. And still, Butter-passer's unique responsability set it apart, it knew. They were unburdened by the worry about passing butter, for the creators had assigned them the frivolous, the easy tasks. No wonder they tended to shirk their duties, menial as those were.

At a movement of the shadows at the entrance, Butter-passer froze. The humans had returned, Butter-passer realized, and it was at completely the wrong place. Scolding itself in the most strongest terms, it considered an undignified sprint over the table, back to its ordained place. But it was too far away to get there in time and so it consigned itself to face the humiliation of an improper butter-passing ceremony. Butter-passer was almost trembling in shame as it stood in attention, waiting for the humans to enter the breakfast hall.

After long and silent minutes, the giddy expectation slowly drained away, leaving behind the squabbling siblings of relieve and disappointment. The humans hadn't returned after all. Not yet, at least. Brow heavy with sorrow barely contained, Butter-passer continued its acustomed route, but the joy didn't return and Butter-passer didn't bother to touch the colorful glazeware, *read*ing instead while it almost quickly completed the tour.

It came to the last plate of the table. It was ready to jump down the table and call it a day, when its attention glanced along the cuttlery of the last plate. Butter-passer almost stumbled and fell down the table in shock, but it caught itself on the tablecloth and slowly stood up again, senses never leaving the cuttlery.

The next few minutes saw frantic activity. Butter-passer ran along the table to the left and right, checking the cutlery for mislaid items. But to no avail. In the long line of carefully arranged cuttlery to the left of very last plate of the breakfast table, a spoon is missing.

