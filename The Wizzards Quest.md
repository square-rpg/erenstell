"Lazit mih giskauwan, waz daz pruoblema ist," Tuk said in its deep and creaking voice and crouched besides the twitching machine, stroking it gently. The Dwarf closed its eyes and started to murmur quitely, then he became siled. While the dwarf sat in silent communion, Vaarharmia mustered the living machine, apparently with a mixture of curiosity and concern. Its once sleek and muscular body now marred by age. Every breath came labouringly, the fleshy sinews of the pump head dry and brittle. While not easily discerned, Vaarharmia saw the signs of anguish and pain on the dwarf's face while it lightly touched the shivering flank.

She herself tried to stay calm and quiet. It was hard, despite all her years of training and meditation. Everybody expected a welkin to be solemn and calm, and she had worked hard to cultivate this image for herself. But inside her feathered breast her heart was beating fast and hard, dread filled her stomach. The water lodger was the last one they had and without it, their flock faced a bleak future.

Just as Erenstell.

The orchard they were standing in had once been a lush and vibrant oasis. An expansive guarden of all kinds of fruit-bearing trees, berry bushes and hedgers, their branches heavy with colorful and succulent fruits. It was a place of abundance and joy, a testament to the ingenuity of the Welkin, to their apparent independence from manna and strom, the Human's gifts that all the other species to thoroughly depended upon.

But now, those once bountyful orchards lay withered with sick and death. For while the welkin had managed to gift a green heart, they had always counted on the Humans to maintain the conditions the gardens had been designed for. And the welking were good botanists and gardeners. They had managed, somehow, over the decades, to learn and adapt. First out of habit and pride and later - when large part of the cities became dependent on the food the garden could provide, out of necessity. They started using human craft and for a while things improved and Erenstell was able to accomodate the folk fleeing the ever more hostile wilderness. For while Erenstell even prospered. But with the humans gone, their machines had started to fail. And the situation was back to dire again.


After a while Tuk opened his eyes, his gaze meeting Vaarharmia's. Over the years she had learned to read the dwarfs expressions. Now she saw deep concern and she dreaded its next words. "Es kan gflyckt werden," it said said, his voice a mix of determination and resignation. "Thoh, siu wirð niht billic siin, Vaarharmia. Thiu lunga deso zitzara ist chrank. Siu mag scôno niht mēr wirken, inti wir müssit siu uuëhsalbōn."


It took Vaarharmia a moment to accomodate the good news. She had been prepared for the worst, given her latest streak of misfortunes. After a moment of perplexed wonder, she clicked her beak.
"I see", she said, hoping the dwarf would take the delayed response as the typical Welkin solemnitude.
